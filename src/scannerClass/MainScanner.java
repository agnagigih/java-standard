package scannerClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MainScanner {
    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);
        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");

        System.out.println("Masukkan Nama : ");
        String nama = scanner.nextLine();
        System.out.println(nama);

        System.out.println("Masukkan Tanggal Lahir : ");
        String tglLahir = scanner.nextLine();
        Date tglLahirDate = formater.parse(tglLahir);
        System.out.println("Tanggal Lahir: "  + tglLahirDate);
    }
}
