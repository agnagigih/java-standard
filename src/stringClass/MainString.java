package stringClass;

public class MainString {
    public static void main(String[] args) {
        String name = "Skadi the Corrupted Heard";
        System.out.println("To Lower Case " + name.toLowerCase());
        System.out.println("To Upper case " + name.toUpperCase());
        System.out.println("Length: " + name.length());
        System.out.println("Start with: " + name.startsWith("Ska"));
        System.out.println("End with: " + name.endsWith("Ska"));

        String[] names = name.split(" ");
        System.out.println("Jumlah kata: " + name.length());
        System.out.println("Index ke-0: " + names[0]);
        System.out.println("Index ke-2: " + names[2]);
    }
}
