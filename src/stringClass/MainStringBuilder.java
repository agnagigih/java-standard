package stringClass;

public class MainStringBuilder {
    public static void main(String[] args) {
        String firstName = "Skadi";
        String lastName = "the Corrupted Heart";
        System.out.println("First name " + firstName);
        System.out.println("Last name " + lastName);

        String namaLengkap = firstName + " " + lastName;
        System.out.println("Nama Lengkap "+namaLengkap);

        StringBuilder builder = new StringBuilder(firstName).append(" ").append(lastName);
        System.out.println("Builder " + builder);
    }
}
