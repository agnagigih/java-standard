package dateCalendarClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class MainDateCalendar {
    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);
        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");

        System.out.println("Masukkan Tanggal Lahir : ");
        String tglLahir = scanner.nextLine();
        Date tglLahirDate = formater.parse(tglLahir);
        System.out.println("Tanggal Lahir: "  + tglLahirDate);

        Date currentDate = new Date();
        String tglSekarang = formater.format(currentDate);
        System.out.println("Tanggla Sekarang Date " + currentDate);
        System.out.println("Tanggal Sekarang " + tglSekarang);

        // hitung usia
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tglLahirDate);
        System.out.println("Kalender Tanggal Lahir " + calendar.getTime());
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(new Date());
        System.out.println("Tanggal Sekarang " + currentCalendar.getTime());
        Long seconds = (currentCalendar.getTimeInMillis() - calendar.getTimeInMillis())/1000;
        System.out.println("Dalam bentuk Second " + seconds);
        Long hours = seconds/3600;
        System.out.println("Dalam bentuk jam : " + hours);
        Long days = hours/24;
        System.out.println("Dalam bentuk hari : " + days);
        Long weeks = days/7;
        System.out.println("Dalam bentuk pekan : " + weeks);
        Long months = days/30;
        System.out.println("Dalam bentuk bulan : " + months);
        Long years = months/12;
        System.out.println("Dalam bentuk tahun : " + years);
        System.out.println("Usia = " + years + " tahun " + (months-years*12) + " bulan");
    }
}
