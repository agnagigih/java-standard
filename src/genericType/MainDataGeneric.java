package genericType;

public class MainDataGeneric {
    public static void main(String[] args) {
        DataGeneric<String> data1 = new DataGeneric<String>();
        data1.setData("FirstName LastName");
        System.out.println("data1 value : " + data1.getData());

        DataGeneric<Integer> data2 = new DataGeneric<Integer>();
        data2.setData(123456);
        System.out.println("data2 value : " + data2.getData());

        DataGeneric<Boolean> data3 = new DataGeneric<Boolean>();
        data3.setData(true);
        System.out.println("data3 value : " + data3.getData());

        DataGeneric<Product> data4 = new DataGeneric<Product>();
        //data4.setData(new Product("Ajinomoto",2500.00));
        Product product = new Product("Produk1", 25000.0);
        data4.setData(product);
        System.out.println("data4 value " +data4.getData());

        DataGeneric<Animal> data5 = new DataGeneric<Animal>();
        data5.setData(new Animal("Hewan1", 4));
        System.out.println("data5 value : " + data5.getData());
        //with constructor
        DataGeneric<Animal> hewan2 = new DataGeneric<>(new Animal("Hewan2", 4));
        System.out.println("hewan1 value : " + hewan2.getData());

        DataGeneric<Car> data6 = new DataGeneric<Car>();
        data6.setData(new Car("Mobil", 250_000_000.00, 1));
        System.out.println("data6 value : " + data6.getData());

    }
}
