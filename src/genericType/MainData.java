package genericType;

public class MainData {
    public static void main(String[] args) {
        Data data = new Data();
        data.setData("FirstName LastName");

        System.out.println("data value: " + data.getData());

        Data data1 = new Data();
        data1.setData(123456);
        System.out.println("data value : " + data1.getData());

        Data data2 = new Data();
        data2.setData(123456.1234);
        System.out.println("data value : " + data2.getData());
    }

}
