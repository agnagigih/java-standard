package genericType;

public class DataGeneric<T> {
    public DataGeneric(){

    }
    public DataGeneric(T data) {
        this.data = data;
    }
    private T data;
    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }

}
